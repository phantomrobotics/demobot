package com.phantom.demobot.modules

import com.phantom.demobot.assemblies.DriveGameAssembly
import labs.phantom.sdk.events.by
import labs.phantom.sdk.events.using
import labs.phantom.sdk.hardware.gamepad.GAMEPAD
import labs.phantom.sdk.hardware.gamepad.Gamepads
import labs.phantom.sdk.hardware.gamepad.sourced
import labs.phantom.sdk.logic.assembly.load
import labs.phantom.sdk.logic.module.GameModule

class DriveGameModule : GameModule(updateable = true) {
    init {
        val drive = DriveGameAssembly()

        assemblies load drive

        this.configure {
            watch by GAMEPAD.events using {
                if(Gamepads.FIRST sourced it) {
                    drive.set(it.pad.left_stick_y, it.pad.right_stick_y)
                }
            }
        }
    }
}