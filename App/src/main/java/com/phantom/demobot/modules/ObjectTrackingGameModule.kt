package com.phantom.demobot.modules

import com.phantom.demobot.assemblies.DriveGameAssembly
import labs.phantom.sdk.events.by
import labs.phantom.sdk.events.using
import labs.phantom.sdk.hardware.log
import labs.phantom.sdk.logic.assembly.load
import labs.phantom.sdk.logic.module.GameModule
import labs.phantom.sdk.tooling.eye.*

class ObjectTrackingGameModule : GameModule() {
    init {
        assemblies load Eye().setup {
            it.parameters.vuforiaLicenseKey = "AfCZvYX/////AAABmWJJ6+YKlUc+hK994eVqGwR7cWHWC5gROtiqJukSYlxB5UYBliJFS6AUvTi9uBSIVGPj5CZwQk8bmzC2Jg3L0km98lO3ZENogt5slQ1HH6Z7Xq2y3TbIuvqssmeeJkDQAu4e+L9dYPuh8+rt/jgDnUH3LbaYYQDe7bWMLugQMu12MPWWb2r4qNepfGE3iJtuuo1hsxLJ+gwvSS3AZekbqvIVPbEznjZrbZnf+5ysG6jdRzaY2QQzcFFa9mzmbn8vyl7l4aRz25fWnrJB30EMLaj5kZ4e28J4vwR8U9+r0PLwsFYrQyM3B8JYxB86nE9FKbAzLc8md8U4GuJGdDrsvSX6KZB/fk1C6kWwcdUtn/dx"
        }

        val drive = DriveGameAssembly()
        assemblies load drive

        this.configure {
            watch by EYE.events using {
                if(it has Minerals.GOLD) {
                    unit.telemetry log "The gold is on the ${it.position.name}"
                    when {
                        it located Position.LEFT -> {
                        }

                        it located Position.RIGHT -> {
                            /// e in dreapta
                        }

                        it located Position.CENTER -> {
                            drive.set(1.0f, 1.0f)
                        }
                    }
                }
            }
        }
    }
}