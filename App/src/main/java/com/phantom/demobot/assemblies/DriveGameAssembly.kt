package com.phantom.demobot.assemblies

import com.qualcomm.robotcore.hardware.DcMotor
import com.qualcomm.robotcore.hardware.DcMotorSimple
import labs.phantom.sdk.hardware.export
import labs.phantom.sdk.logic.assembly.GameAssembly

class DriveGameAssembly : GameAssembly() {
    lateinit var leftMotor: DcMotor
    lateinit var rightMotor: DcMotor

    init {
        this.configure {
            it.initialized {
                leftMotor = hardware export "left_motor"
                rightMotor = hardware export "right_motor"

                leftMotor.direction = DcMotorSimple.Direction.REVERSE
            }

            it.started {
                stopMotors()
            }

            it.stopped {
                stopMotors()
            }
        }
    }

    public fun set(left: Float, right: Float) {
        leftMotor.power = left.toDouble()
        rightMotor.power = right.toDouble()
    }

    public fun stopMotors() {
        leftMotor.power = 0.0
        rightMotor.power = 0.0
    }
}