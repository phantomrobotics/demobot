package com.phantom.demobot.units

import com.phantom.demobot.modules.DriveGameModule
import com.qualcomm.robotcore.eventloop.opmode.TeleOp
import com.qualcomm.robotcore.hardware.Servo
import com.qualcomm.robotcore.hardware.VoltageSensor
import labs.phantom.sdk.hardware.export
import labs.phantom.sdk.hardware.gamepad.GamepadModule
import labs.phantom.sdk.logic.GameUnit
import labs.phantom.sdk.logic.module.load

@TeleOp(name = "TestGameUnit")
class TestGameUnit : GameUnit(updateable = true) {
    lateinit var voltageSensor: VoltageSensor

    init {
        modules load GamepadModule()
        modules load DriveGameModule()

        this.configure {
            it.initialized {
                voltageSensor = hardware export "Motor Controller 1"
            }

            it.updated {
                if(voltageSensor.voltage < 11) {
                    it.stop();
                }
            }
        }
    }
}