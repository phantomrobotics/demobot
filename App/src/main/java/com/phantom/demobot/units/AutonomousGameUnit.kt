package com.phantom.demobot.units

import com.phantom.demobot.modules.ObjectTrackingGameModule
import com.qualcomm.robotcore.eventloop.opmode.Autonomous
import labs.phantom.sdk.logic.GameUnit
import labs.phantom.sdk.logic.module.load

@Autonomous(name = "AutonomousGameUnit")
class AutonomousGameUnit : GameUnit() {
    init {
        modules load ObjectTrackingGameModule()
    }
}