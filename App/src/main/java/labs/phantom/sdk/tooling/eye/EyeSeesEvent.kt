package labs.phantom.sdk.tooling.eye

import labs.phantom.sdk.events.GameEvent
import labs.phantom.sdk.events.GameEventEmitter

class EyeSeesEvent(public val eye: Eye, public val mineral: Minerals, public val position: Position): GameEvent() { }

class EyeSeesEventEmitter: GameEventEmitter<EyeSeesEvent>() {}

object EYE {
    public var events = EyeSeesEventEmitter()
}