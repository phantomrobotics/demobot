package labs.phantom.sdk.tooling.eye

typealias EyeSetupCallback = (u: Eye) -> Unit
typealias EyeSeesCallback = (u: EyeSeesEvent) -> Unit


infix fun EyeSeesEvent.has(mineral: Minerals): Boolean {
    return this.mineral.name == mineral.name
}
infix fun EyeSeesEvent.located(position: Position): Boolean {
    return this.position.name == position.name
}