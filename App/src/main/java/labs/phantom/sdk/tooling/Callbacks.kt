package labs.phantom.sdk.tooling

import labs.phantom.sdk.logic.GameUnit

typealias VoidCallback = () -> Unit
typealias ConfigureCallback = (u: GameUnit) -> Unit
typealias ParameterizedCallback<T> = (u: T) -> Unit