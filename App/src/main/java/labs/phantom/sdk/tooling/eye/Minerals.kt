package labs.phantom.sdk.tooling.eye

enum class Minerals(name: String) {
    GOLD("Gold Mineral"),
    SILVER("Silver Mineral")
}