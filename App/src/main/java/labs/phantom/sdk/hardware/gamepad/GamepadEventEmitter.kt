package labs.phantom.sdk.hardware.gamepad

import labs.phantom.sdk.events.GameEventEmitter

class GamepadEventEmitter: GameEventEmitter<GamepadEvent>() {}