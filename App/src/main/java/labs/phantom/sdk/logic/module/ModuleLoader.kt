package labs.phantom.sdk.logic.module

import labs.phantom.sdk.logic.GameUnit

class ModuleLoader(public val unit: GameUnit) { }

infix fun ModuleLoader.load(that: GameModule) {
    this.unit.addModule(that)
}