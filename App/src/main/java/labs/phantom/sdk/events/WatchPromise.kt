package labs.phantom.sdk.events

class WatchPromise<T: GameEvent>(public val emitter: GameEventEmitter<T>) { }